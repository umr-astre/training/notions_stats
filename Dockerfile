# Reproducible environment
# Source for the docker image registry.forgemia.inra.fr/umr-astre/training/notions_stats
# (c) 2020 Facundo Muñoz

# Usage:
# - Reproduce CI locally:
#   sudo docker pull registry.forgemia.inra.fr/umr-astre/training/notions_stats
#   sudo docker run -e PASSWORD=fmstudio -p 8787:8787 --rm -it -v $(pwd):/home/rstudio/notions_stats registry.forgemia.inra.fr/umr-astre/training/notions_stats  # Run from the project root dir.
#   [browse to localhost:8787] username: rstudio, password: fmstudio
#   rmarkdown::render_site("src")
#
# - Modify and update Docker image
#   sudo docker build -t registry.forgemia.inra.fr/umr-astre/training/notions_stats .
#   [test]
#   sudo docker push registry.forgemia.inra.fr/umr-astre/training/notions_stats


FROM rocker/r-rmd

LABEL maintainer="Facundo Muñoz facundo.munoz@cirad.fr"

# Install external dependencies

RUN export DEBIAN_FRONTEND=noninteractive; apt-get -qq update \
&& apt-get install -y --no-install-recommends \
libssl-dev \
libxml2-dev \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/

# Install R-package dependencies for compiling reports
RUN ["install2.r", \
"distill", \
"hrbrthemes", \
"janitor", \
"metathis", \
"remotes", \
"xaringan" \
]

RUN Rscript -e 'remotes::install_github("gadenbuie/xaringanExtra")'

# Set up environment
RUN echo 'alias ll="ls -lh --color=tty"' >> ~/.bashrc

WORKDIR /home/rstudio