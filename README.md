
<!-- badges: start -->
<!-- badges: end -->

![](src/img/course_logo.png)

Fichiers source pour les matériaux du stage de formation.

[Site web](https://umr-astre.pages.mia.inra.fr/training/notions_stats/) de la formation créé à l'aide du package R [distill](https://rstudio.github.io/distill/) et inspiré des idées de l'atelier RStudio [_Teaching in Production_](https://alison.rbind.io/project/rstudio-tip/) développé par [Alison Hill](https://alison.rbind.io/).

Diapositives créées à l'aide du package R [**xaringan**](https://github.com/yihui/xaringan), en s'appuyant sur [remark.js](https://remarkjs.com), [**knitr**](https://yihui.org/knitr), et [R Markdown](https://rmarkdown.rstudio.com).

