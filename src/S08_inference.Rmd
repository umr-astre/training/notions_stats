---
title: "Inférence statistique classique"
description: |
  Interpréter les résultats d’un modèle de régression linéaire et des tests statistiques classiques.
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```


```{r meta, echo=FALSE}
library(metathis)
meta() %>%
  meta_general(
    description = "Stage de formation. Cirad. Mai 2023.",
    generator = "xaringan and remark.js"
  ) %>% 
  meta_name("gitlab-repo" = "https://forgemia.inra.fr/umr-astre/training/notions_stats") %>% 
  meta_social(
    title = "Notions de base en statistiques",
    url = "https://umr-astre.pages.mia.inra.fr/training/notions_stats/",
    image = "https://umr-astre.pages.mia.inra.fr/training/notions_stats/img/course_icon.svg",
    image_alt = "Logo formation",
    og_type = "website",
    twitter_card_type = "summary_large_image"
  )
```


# Intervalles de Confiance et Tests d'Hypothèse

Distribution d'échantillonnage. Intervalles de confiance. Signification. Puissance.

```{r share-again}
xaringanExtra::use_share_again()
```

```{r embed-xaringan-icnhst, fig.cap="[Slides](slides/S8.1_ICs-NHST.html)"}
xaringanExtra::embed_xaringan(url = "slides/S8.1_ICs-NHST.html", ratio = "16:9")
```




# Conclusions et interprétations de résultats 

Corrélation et causalité. Confusion, biais et degrés de liberté des chercheurs.

```{r embed-xaringan-interpretation, fig.cap="[Slides](slides/S8.2_interpretation.html)"}
xaringanExtra::embed_xaringan(url = "slides/S8.2_interpretation.html", ratio = "16:9")
```



# Clôture de la formation


```{r embed-xaringan-cloture, fig.cap="[Slides](slides/S9.1_cloture.html)"}
xaringanExtra::embed_xaringan(url = "slides/S9.1_cloture.html", ratio = "16:9")
```




