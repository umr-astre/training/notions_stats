# Séquence 1.1. Questionnaire initial.
## Positionnement de départ

Temps estimé : 5'

Sauf indication contraire, les réponses à toutes les questions seront proposées sur une échelle de Likert à 6 niveaux :

1-6: Très en désaccord, en désaccord, plutôt en désaccord, plutôt d'accord, d'accord, très d'accord.


Quessionnaire initial
1.J'ai confiance de maîtriser les contenus de cette formation
Très en désaccord
Très d'accord
2.J'espère réussir la formation
3.Cette formation me sera d'utilité pour mon travail
4.Je pense que cette formation sera intéressante
5.Réussir cette formation est important pour moi
6.En général je ressens de l'anxiété envers les mathématiques
7.J'ai des difficultés pour saisir les matériaux en anglais
8.J'espère creuser d'avantage ma formation en statistiques après cette formation
9.Concernant cette formation, ce qui m'inquiète est... ("rien du tout" est possible)

10.Si vous le souhaitez, identifiez-vous pour récupérer vos réponses à la fin de la formation.
