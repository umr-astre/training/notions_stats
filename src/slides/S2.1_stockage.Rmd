---
title: "Le stockage de données"
author: "Facundo Muñoz<br/>facundo.munoz@cirad.fr<br/>"
institute: "<img src=\"img/CirBlanc_L230px.png\" style=\"width: 25%\" align=\"top\" />"
date: "`r format(Sys.Date())`"
output:
  xaringan::moon_reader:
    # css: ["default", "assets/css/my-theme.css", "assets/css/my-fonts.css"]
    css: ["default", "default-fonts", "libs/font-awesome/css/fontawesome-all.min.css", "assets/css/cirad.css"]
    seal: false
    lib_dir: libs
    nature:
      slideNumberFormat: |
        <div class="progress-bar-container">
          <div class="progress-bar" style="width: calc(%current% / %total% * 100%);">
          </div>
        </div>
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: "16:9"
---


```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
knitr::opts_chunk$set(collapse = TRUE, fig.retina = 3)
# library(ymlthis)
library(tidyverse)
library(xaringanExtra)
# library(palmerpenguins)
# xaringanExtra::use_panelset()
xaringanExtra::use_share_again()
xaringanExtra::style_share_again(
  share_buttons = c("twitter", "linkedin", "pocket")
)
```

```{r broadcast, echo=FALSE}
xaringanExtra::use_broadcast()
```


class: title-slide, inverse

.pull-left[
# `r rmarkdown::metadata$title`

## `r rmarkdown::metadata$subtitle`

### `r rmarkdown::metadata$author` 

![](img/CirBlanc_L230px.png)
]
.pull-right[
![](https://datacarpentry.org/rr-organization1/fig/files_messy_tidy.png)
]
???

---
layout: true

<a class="footer-link" href="https://umr-astre.pages.mia.inra.fr/training/notions_stats/">Notions de base en statistiques - umr-astre.pages.mia.inra.fr/training/notions_stats/</a>


<!-- --- -->

<!-- <iframe width="800px" height="480px" src="https://forms.office.com/Pages/ResponsePage.aspx?id=kF-vYLTj1keMjubW0rEXuQZjSr6_pKhPjGFg7OvEV3dUQlhGTTJTV0RYMFVCNjVONFZaTVU5OFI0VyQlQCN0PWcu&embed=true" frameborder="0" marginwidth="0" marginheight="0" style="border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe> -->

<!-- --- -->

<!-- <iframe width="800px" height= "480px" src= "https://forms.office.com/Pages/AnalysisPage.aspx?AnalyzerToken=woDjclJbLhRmhkC9uo2axQG6N9JHElTb&id=kF-vYLTj1keMjubW0rEXuQZjSr6_pKhPjGFg7OvEV3dUQlhGTTJTV0RYMFVCNjVONFZaTVU5OFI0VyQlQCN0PWcu" frameborder= "0" marginwidth= "0" marginheight= "0" style= "border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe> -->


---
<!-- class: inverse, middle, center -->

<!-- # Quels problèmes avez-vous rencontré concernant les fichiers de données ? -->

<!-- ??? -->

<!-- Dites-moi si les problèmes suivantes vous parlent -->
# Quelques problèmes courants

---

- J'ai plusieurs __copies des données__ mais je ne suis pas sûr quelle est la dernière version.

- J'ai les données, mais je ne suis pas sûr d'avoir la __dernière version__.

- J'ai un fichier de données mais je ne me souviens pas de ce qu'il __contient__.

- J'ai des données, mais je ne les __retrouve__ pas.

- Je ne sais pas comment interpréter certaines des __variables__.

- J'ai corrigé des erreurs et on m'a envoyé une __nouvelle version__ basée sur les données originales

---
background-image: url(https://media.giphy.com/media/TKvErZACqjawXcTMSP/giphy.gif)
background-size: cover
class: middle, inverse

# Pertes de temps

# Erreurs inaperçues


---
# Quelques principes

1. ~~Limiter~~ Éviter la __duplication__ des données

--

2. Gérer les __versions__

--

3. __Documenter__ les données (méta-données)

--

4. Adopter une convention pour les __noms des fichiers__


.center[
  # Entamez un document de _guidelines_ à respecter
]

???

- Guide pour nos actions. Pas toujours possible, mais peuvent nous aider à prendre des décisions.

- Mieux se référer à un dépôt central de données qu'envoyer des copies par é-mail

- Les données peuvent évoluer (ajouter, corriger). Utilisez __un__ système quelconque. Soit un outil, ou une procédure.
Une date dans le nom du fichier peut être suffisant. Mais il faut être __consistent__ et __systèmatique__. 

- Un fichier de texte à coté, par exemple, ou un outil (dépôt, cf. 1). Description des variables, source des données, etc.

- Réfléchissez au noms des fichiers (et variables) pour qu'ils aient du sens, qui reflètent leur contenu, qui soient parlants. 

Chaque situation est différente, je ne peux pas vous proposer un système qui vous convient à tous. 

__Le choix du système n'est pas aussi important que d'en avoir un.__


---
background-image: url(img/spreadsheet.png)
background-size: 50%
background-position: right

# Feuilles de calcul

.pull-left[
Combinent : 

- stockage de données

- entrée de données

- visualisation (tables, format)

- analyses (formules, conditions, résultats, résumés, etc.)

- figures
]

???

- J'adore les feuilles de calcul, je pense sincèrement que sont un outil fantastique

- Mais je ne les utilise pas ni pour le stockage ni pour la manipulation de données



---
class: inverse, center, middle

# Les besoins pour l'__entree__, le __stockage__ et la __visualisation__ de données sont fondamentalement différents

???

- Les données stockées doivent être touchés le moins possible pour minimiser le risque de modification accidentel

---
# Histoires d'horreur

- MS Excel interprète les dates et les garde internement comme un nombre... avec des conventions différents pour Mac et Windows [`r icons::fontawesome("link")`](https://uc3.cdlib.org/2014/04/09/abandon-all-hope-ye-who-enter-dates-in-excel/)


- MS Excel interprète _automatiquement_ certaines textes comme des dates. E.g. le symbole pour le gène "Oct-4" peut être écrasé sans notification.

  [Un étude de 2016](https://doi.org/10.1186/s13059-016-1044-7) à trouvé ce type d'__erreurs dans 20 %__ des listes des gènes publiées.



???

- Excel a la mauvaise habitude d'interpréter les données (pour améliorer la visualisation) mais en les __changeant__ au passage.

- L'interprétation des données peuvent dépendre du système d'exploitation, du langage du système et d'autres paramètres.

---
background-image: url(img/gene_renames.png)
background-size: contain
background-position: center

---
class: middle
# 

Optimist: The glass is 1/2 full.

Pessimist: The glass is 1/2 empty.

Excel: The glass is January 2nd.

---
class: middle, center
background-image: url(img/and-thats-why-excel.jpg)
background-size: contain
background-position: center


---
class: inverse, center, middle

# Vous allez utiliser les outils que vous __maîtrisez__, pas forcement ceux dont vous avez __besoin__

--

Vous pouvez les utiliser, mais pensez aux __principes__ de gestion de données

???

Ok. C'est normal. Je comprends.

Vous pouvez faire un analyse sur Excel, mais faites-les sur une feuille à part. Ne touchez pas les données. C'est mieux d'utiliser un fichier à part (viol de la duplication de données !!, bon... tant pis). 


---
class: inverse, center, middle

# Noms des fichiers (et variables)

.pull-right[.quote[
> ‘There are only two hard things in Computer Science: cache invalidation and naming things.’
>
>    - Phil Karlton
]
]

---
background-image: url(http://phdcomics.com/comics/archive/phd101212s.gif)
background-position: center

---

# No

- myabstract.docx
- Les noms de fichiers d'Agnès utilisent les espaces et la  ponctuation.xlsx
- figure I.png
- fig 2.png
- JW7d^(2sl@nepassupprimerWx2*.txt

--

# Oui

- 2014-06-08_abstract-for-sla.docx
- les-fichiers-dagnes-vont-mieux.xlsx
- fig01_nuage-points-taille-vs-interet.png
- fig02_histogramme-participation.png
- 1986-01-28_donnees-brutes-projet-code.txt

---
# 3 principes pour les noms (de fichiers)

1. Lisibles par une machine

--

2. Lisibles par les humains

--

3. Fonctionne bien avec l'ordre d'affichage

???

- Éviter accents, ponctuation, espaces.

- Utiliser une structure régulière, systématique

- délimiter mots et "groupes" avec - et _

- Dates en format standard ISO-8601

- machine : facilite la recherche de fichiers, et l'extraction de méta-données à partir des noms de fichiers.

- humain : contient info sur les contenus



---
background-image: url(https://imgs.xkcd.com/comics/iso_8601_2x.png)

.pull-right[
  [Format standard ISO 8601](https://en.wikipedia.org/wiki/ISO_8601)
]

---
class: middle, center

# Excellents noms de fichiers

![](https://datacarpentry.org/rr-organization1/fig/awesome_names.png)

.credit[[datacarpentry]()]
---
# Nommer des fichiers (et des variables)

Des outils d'aide pour développer une __nomenclature__ de fichiers adaptée à vos besoins :

- Kristin Briney (2020) [File naming convention worksheet](http://dataabinitio.com/?p=976)

- [Minnesota Historical Society](https://www.mnhs.org/preserve/records/electronicrecords/erfnaming.php#planning)

- [University of Edinburgh, Records Management](https://www.ed.ac.uk/records-management/guidance/records/practical-guidance/naming-conventions)

---
class: middle, center, inverse

# Organiser ses fichiers

---
background-image: url(https://datacarpentry.org/rr-organization1/fig/files_messy_tidy.png)
background-position: center

.credit[datacarpentry]

---
# Mon structure de projet basique

.left-column[
```{r eval=FALSE}
.
├── data/
├── doc/
├── reports/
├── src/
└── Readme.md
```
]

???

Celle-là est la mienne. Pas forcement adapté à vos besoins.
C'est un exemple.

Selon le cas, il peut y avoir des répertoires ou fichiers supplémentaires.

E.g. raw_data, derived_data, img/figures, paper

---
# Principes

1. Je ne modifie __jamais__ les fichiers dans `data`

2. Toute la __documentation__ à lire dans `doc` (e.g. description des données, articles, etc.)

3. Le travail d'__analyse__ se passe dans `src`

4. Les __résultats__ dans `reports`


???

Peu importe le système. Mais en avoir un.

---
# Références

- Kristin Briney (2020) [File naming convention worksheet](http://dataabinitio.com/?p=976)

- Data Carpentry (2018) [lesson on file organisation](https://datacarpentry.org/rr-organization1/)

- Karl W. Broman & Kara H. Woo (2018). Data organisation in Spreadsheets. _The American Statistician_, 72:1, 2-10, [DOI: 10.1080/00031305.2017.1375989](https://doi.org/10.1080/00031305.2017.1375989)

  

???




---
class: middle

# Merci!

Diapositives créées à l'aide du package R [**xaringan**](https://github.com/yihui/xaringan).

En s'appuyant sur [remark.js](https://remarkjs.com), [**knitr**](https://yihui.org/knitr), et [R Markdown](https://rmarkdown.rstudio.com).

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
