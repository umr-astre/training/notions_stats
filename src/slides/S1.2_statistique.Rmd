---
title: "Qu'est-ce que la Statistique"
subtitle: "Vocabulaire de base"
author: "Facundo Muñoz<br/>facundo.munoz@cirad.fr<br/>"
institute: "<img src=\"img/CirBlanc_L230px.png\" style=\"width: 25%\" align=\"top\" />"
date: "`r format(Sys.Date())`"
output:
  xaringan::moon_reader:
    # css: ["default", "assets/css/my-theme.css", "assets/css/my-fonts.css"]
    css: ["default", "default-fonts", "libs/font-awesome/css/fontawesome-all.min.css", "assets/css/cirad.css"]
    seal: false
    lib_dir: libs
    nature:
      slideNumberFormat: |
        <div class="progress-bar-container">
          <div class="progress-bar" style="width: calc(%current% / %total% * 100%);">
          </div>
        </div>
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: "16:9"
---


```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
knitr::opts_chunk$set(collapse = TRUE, fig.retina = 3)
# library(ymlthis)
library(tidyverse)
library(xaringanExtra)
# library(palmerpenguins)
# xaringanExtra::use_panelset()
xaringanExtra::use_share_again()
xaringanExtra::style_share_again(
  share_buttons = c("twitter", "linkedin", "pocket")
)
```

```{r broadcast, echo=FALSE}
xaringanExtra::use_broadcast()
```

class: title-slide, inverse

.pull-left[
# `r rmarkdown::metadata$title`

## `r rmarkdown::metadata$subtitle`

### `r rmarkdown::metadata$author` 

![](img/CirBlanc_L230px.png)
]
.pull-right[
![](img/statistics_wordcloud.jpeg)
]
???

Comme introduction, nous allons commencer par clarifier quelques termes et concepts.

---
layout: true

<a class="footer-link" href="https://umr-astre.pages.mia.inra.fr/training/notions_stats/">Notions de base en statistiques - umr-astre.pages.mia.inra.fr/training/notions_stats/</a>

---
class: inverse, center, middle

# Vocabulaire

Quels termes ou concepts associez-vous au mot __Statistique__ ?


???

- Ouvrir le fichier `src/S1.2_Vocabulaire-rélations.graphml` avec yEd. 
- Colorier les termes évoqués et ajouter les nouveaux.
- Exporter sur `slides/img/S1.2_Vocabulaire-rélations.svg` et pusher le repo pour mettre à jour le diagramme.


---

# Diagramme conceptuel

![](img/S1.2_Vocabulaire-rélations.svg)

???

- Discuter sur le diagramme final.

- Noter les deux "camps" : théorique et observé. Continuer avec la prochaine.


---
background-image: url(img/iceberg-statistics.jpg)
background-size: contain
background-position: center

---

# Statistique

--

> Discipline qui étudie comment extraire des __conclusions__ à partir d'__observations__ avec de l'__incertitude__

.center[
![](img/S1.2_statistique_fig-1.webp)
]

???

Mais, qu'est-ce que c'est, essentiellement, la Statistique ?

---
# Exemple

## Quel est le stock total de Maquereau en Méditerranée ?

.center[
  ![](img/S1.2_statistique_fig-2.webp)
]

???

C'est une question importante, pour la préservation.

--

- Impossible de répondre à cette question avec __certitude__ !

???

Mais on a quelques pistes

--

- __Observations__ : abondance de la pêche (stock `r icons::fontawesome("arrow-right")` abondance)

???

- Au moins il doit y avoir tout ce qui est pêché (et un peu plus aussi). 

- Plus le stock est important, plus la pêche est abondante.


--


- __Variabilité__ : outre le stock, de nombreux facteurs influencent l'abondance de la pêche 

???

Mais la relation n'est pas exacte : outre le stock, de nombreux facteurs influencent l'abondance de la pêche. 

Pouvez-vous nommer quelques uns ?

-  Le lieu, le climat, le mouvement des poissons, la chance ! 

Nous avons donc des observations qui entretiennent une relation __approximative__ ou incertaine avec ce qui nous intéresse : le stock de poissons.

---
class: center, middle

# Voici l'astuce fondamentale :

comme nous ne pouvons pas contrôler ou mesurer tous les facteurs supplémentaires qui influencent les observations

# nous considérerons que l'effet conjoint de tous ces facteurs est une __variation aléatoire__ (c'est-à-dire imprévisible) sur la pêche.

???

Dans d'autres cas, comme l'échantillonnage aléatoire pour une enquête électorale, ou pour une expérience scientifique, il y a un vraie aléatoriété induite par le protocole. 


---
# Conclusion

En posant quelques __hypothèses__ générales sur la _forme_ de ces variations (à l'aide de __distributions de probabilités__), les statistiques nous permettent d'extraire des __informations__ utiles sur ce qui nous intéresse.
Mais attention : 

.center[
## l'__incertitude__ est toujours là, 
]

- conclusions exprimées termes __probabilistes__

> "Le stock total sera très probablement ici en attendant" ; "Il y aura très probablement plus d'abondance ici que là-bas" ; ...

---
class: middle

# Merci!

Diapositives créées à l'aide du package R [**xaringan**](https://github.com/yihui/xaringan).

En s'appuyant sur [remark.js](https://remarkjs.com), [**knitr**](https://yihui.org/knitr), et [R Markdown](https://rmarkdown.rstudio.com).

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
