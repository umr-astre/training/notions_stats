---
title: "Paramètres descriptifs des variables"
author: "Facundo Muñoz<br/>facundo.munoz@cirad.fr<br/>"
institute: "<img src=\"img/CirBlanc_L230px.png\" style=\"width: 25%\" align=\"top\" />"
date: "`r format(Sys.Date())`"
output:
  xaringan::moon_reader:
    # css: ["default", "assets/css/my-theme.css", "assets/css/my-fonts.css"]
    css: ["default", "default-fonts", "libs/font-awesome/css/fontawesome-all.min.css", "assets/css/cirad.css"]
    seal: false
    lib_dir: libs
    nature:
      slideNumberFormat: |
        <div class="progress-bar-container">
          <div class="progress-bar" style="width: calc(%current% / %total% * 100%);">
          </div>
        </div>
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: "16:9"
      beforeInit: "macros.js"
---


```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
knitr::opts_chunk$set(
  collapse = TRUE,
  echo = FALSE,
  fig.retina = 3,
  fig.width=5,
  fig.height=3,
  out.width='90%'
)
# library(ymlthis)
library(tidyverse)
library(xaringanExtra)
# library(palmerpenguins)
# xaringanExtra::use_panelset()
xaringanExtra::use_share_again()
xaringanExtra::style_share_again(
  share_buttons = c("twitter", "linkedin", "pocket")
)

theme_set(theme_ipsum())
```

```{r broadcast, echo=FALSE}
xaringanExtra::use_broadcast()
```


class: title-slide, inverse

.pull-left[
# `r rmarkdown::metadata$title`

## `r rmarkdown::metadata$subtitle`

### `r rmarkdown::metadata$author` 

![](img/CirBlanc_L230px.png)
]


.pull-right[
![](img/centralities.gif)
]

???


```{r load}
aflmargins <- read.csv(
  "~/.jamovi/modules/lsj-data/data/aflsmall_margins.csv"
)
```

---
layout: true

<a class="footer-link" href="https://umr-astre.pages.mia.inra.fr/training/notions_stats/">Notions de base en statistiques - umr-astre.pages.mia.inra.fr/training/notions_stats/</a>


---
# AFL margins

Marge gagnante (nombre de points) pour les 176 matchs de la Ligue Australiènne de Football (AFL) joués à domicile et à l’extérieur durant la saison 2010.

.small[
```{r results='asis'}
cat(paste(aflmargins$afl.margins, collapse = ", "))
```
]

???

Qu'est-ce que vous pouvez me dire sur les valeurs de cette variable ?

Comment pourriez-vous résumer l'information contenue dans cette liste ?


---
# Le concepte de distribution

```{r}
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 1)

```

???

On peut commencer pour regarder le nombre total d'observations (`r nrow(aflmargins)`) et leur rang (`r aflmargins %>% with(., c(min(afl.margins), max(afl.margins))) %>% paste(collapse = ", ")`).


---
# Le concepte de distribution

```{r}
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 3, boundary = 0)

```

???

On regardant de plus près la liste de valeurs, on remarque que il y a relativement peu de valeurs "grands" (près de la borne supérieur) par rapport aux valeurs "petits".

Autrement dit, que la répartition de valeurs n'est pas __uniforme__ dans ce rang
de valeurs.

Pour être un peu plus exactes, on précise qu'il y a presque 150 valeurs entre
0 et 60 et environ 26 entre 60 et 116.


---
# Le concepte de distribution

```{r}
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 5, boundary = 0)

```

???

Tant qu'on y est, on peut décider d'affiner un peu plus et décrire le nombre
d'observations sur des intervalles de plus en plus fins.



---
# Le concepte de distribution

```{r}
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 9, boundary = 0)

```

???

Mais au même temps, on résume de moins en moins


---
# Le concepte de distribution

```{r}
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 17, boundary = 0)

```

???

Mais au même temps, on résume de moins en moins


---
# Le concepte de distribution

```{r}
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 33, boundary = 0)

```

???

Et on commence à trouver des intervalles dans lequels il n'y a pas eu 
aucune observation.

__Pour quoi__ à votre avis ?

---
# Le concepte de distribution

```{r}
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(binwidth = 1, boundary = 0)

```

???

À la limite, on décrit le nombre d'observations pour chaque possible valeur :
6 zéros, 2 uns, 8 deux, etc.

Êtes-vous d'accord avec moi qu'un point intermédiaire est plus utile ?


Ce graphique s'appelle __histogramme__ et représente la __distribution__ ou __répartition empirique__ de la variable.

__Empirique__ parce que c'est ce qui a été observé dans notre échantillon.

En contraposition à la distribution __théorique__ : ce qu'on obtiendrait si on observait une infinité de matchs, et qui lisse les petites fluctuations dues au
hasard.



---
class: inverse, middle, center

# Mesures de tendance centrale


???

Souvent il est utile de condenser les données en quelques statistiques « sommaires » simples. Dans la plupart des situations, la première chose que vous voudrez calculer est une mesure de la __tendance centrale__. 

En d’autres termes, vous aimeriez savoir où se situe le « milieu » de vos données.

---

# Le __centre__ d'une distribution

```{r}
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 13, boundary = 0)
```



???

Où situeriez vous le "centre" de ces données ?




---

# Moyenne

```{r central-pars}
central_pars <- with(
  aflmargins,
  c(mean = mean(afl.margins),
    median = median(afl.margins),
    mode = aflmargins %>% count(afl.margins) %>% slice_max(n) %>% pull(afl.margins))
)
```

<!-- $$\bar X = \frac1N \sum_{i=1}^N X_i$$ -->

```{r }
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 13, boundary = 0) +
  geom_vline(xintercept = central_pars["mean"])
```



???

C'est la moyenne arithmétique tradicionnelle.

Si on repartissiez la somme totale uniformement entre toutes les observations,
chaque une seriez de ~ 35 points.

Le __centre de gravité__ de la distribution : la somme des écarts est 0.


---

# Medianne


```{r }
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 13, boundary = 0) +
  geom_vline(xintercept = central_pars["median"])
```



???

La valeur __centrale__ (autant d'observations en dessous qu'on dessus).

Correspond à la valeur qui sépare la __surface__ de la distribution en deux.




---

# Mode


```{r }
aflmargins %>% 
  ggplot(aes(afl.margins)) +
  # geom_dotplot(binwidth = 1, method = "histodot")
  geom_histogram(bins = 13, boundary = 0) +
  geom_vline(xintercept = central_pars["mode"])
```



???

La valeur __la plus fréquente__ (`r central_pars["mode"]`, en l'occurrence).

Observez que dans ce cas, c'est une valeur extrême plutôt que centrale, à cause de la _répartition_ particulière des valeurs.



---
class: inverse, middle, center

# Quelle mesure utiliser, quand ?




---
# Quelques indications


| Échelle de mesure  | Moyenne | Médiane | Mode |
|--------------------|:-------:|:-------:|:----:|
| Nominale (espèce)  |         |         |      |
| Ordinale (score)   |         |         |      |
| Quantitative (age) |         |         |      |



---
# Quelques indications


| Échelle de mesure  | Moyenne | Médiane | Mode |
|--------------------|:-------:|:-------:|:----:|
| Nominale (espèce)  |         |         |  🗹  |
| Ordinale (score)   |         |    🗹   |  🗹  |
| Quantitative (age) |    🗹   |    🗹   |  🗹  |



---
# Rélacion entre les paramètres 


![:scale 150%](img/centralities.gif)


???

# Lexique

Positive skew = right skewed = asymmetrie positive / vers la droite


---
# Discussion

Quelle information apporte une mesure de centralité pour la variable suivante :

```{r}
tibble(
  x = c(rnorm(1e3), rnorm(.7e3, 4))
) %>% 
  ggplot(aes(x)) +
  geom_histogram(bins = 21)
```

???

Une mesure de centralité est utile pour les distributions unimodales. Autrement, 
peuvent être trompeuses.


---
class: inverse, middle, center

# Dispersion

## Quantifier la __variabilité__ autour de la valeur _centrale_

???

Comment les données sont-elles _étalées_ ? A quelle distance du centre elles se trouvent typiquement ?

---

# Mesures typiques

- Étendue (fourchette) : écart maximale entre observations (max - min)

- Écart interquartile (IQR) : $Q_3 - Q_1$ (_moitié centrale_ des données)

- Variance : moyenne des écarts (par rapport à la moyenne) carrés

- Écart type (ET) : Racine de la variance (échelle de la variable)


???

l'IQR est comme l'étendue mais plus robuste (moins sensible à des valeurs aberrants)

Noter que la moyenne (la somme) des écarts est zéro, par définition.
L'Écart type cherche à caractériser l'écart _typique_.

On pourrait utiliser aussi l'écart __absolut__ moyen ou encore __médian__ (par rapport à la médiane).

Mais, mathématiquement, il y a de bonnes raisons de préférer les écarts au carré aux écarts absolus. Notamment, l'additivité : Var(X + Y) = Var(X) + Var(Y)


---

# Exercice

.middle[
  - Dans __jamovi__, explorer les analyses descriptives des variables sur le jeu
de données __Clinical Trial__.

  - Décrivez la variable `mood.gain`, en séparant par médicament (`Split by`)
  
  - Pensez-vous que le nouveau traitement est plus efficace ?
  
]


---
class: inverse, middle, center

# Association entre deux variables continues

```{r pts}
set.seed(20210114)
rho <- 0.8
N <- 8
U <- chol(matrix(c(1, rho, rho, 1), 2, 2))

pts <- as_tibble(
  structure(
    round(10 * matrix(rnorm(2*8), N, 2)) %*% U,
    dimnames = list(NULL, c("x", "y"))
  )
)
```


---
# Diagramme de dispersion (scatter plot)

.left-column[
```{r }
kable(pts)
```

]

.right-column[
```{r }
ggplot(pts)
```

]


---
# Diagramme de dispersion (scatter plot)

.left-column[
```{r }
kable(pts)
```

]

.right-column[
```{r }
ggplot(pts) +
  aes(x = x)
```

]


---
# Diagramme de dispersion (scatter plot)

.left-column[
```{r }
kable(pts)
```

]

.right-column[
```{r }
ggplot(pts) +
  aes(x = x) +
  aes(y = y) + geom_point(alpha = 0)
```

]


---
# Diagramme de dispersion (scatter plot)

.left-column[
```{r }
kable(pts) %>% 
  row_spec(1, background = "lightblue")
```

]

.right-column[
```{r }
ggplot(pts) +
  aes(x = x) +
  aes(y = y) + geom_point(alpha = 0) +
  geom_point(data = function(x) slice(x, 1), color = "lightblue")
```

]


---
# Diagramme de dispersion (scatter plot)

.left-column[
```{r }
kable(pts) %>% 
  row_spec(2, background = "lightblue")
```

]

.right-column[
```{r }
ggplot(pts) +
  aes(x = x) +
  aes(y = y) + geom_point(alpha = 0) +
  geom_point(data = function(x) slice(x, 1)) +
  geom_point(data = function(x) slice(x, 2), color = "lightblue")
```

]


---
# Diagramme de dispersion (scatter plot)

.left-column[

```{r }
kable(pts)
```

]

.right-column[
```{r }
ggplot(pts) +
  aes(x = x) +
  aes(y = y) + geom_point(alpha = 0) +
  geom_point(data = function(x) slice(x, 1)) +
  geom_point(data = function(x) slice(x, 2)) +
  geom_point()
```

]



---
# Interprétation de la corrélation

<iframe width="960px" height= "480px" src= "https://rpsychologist.com/correlation/" frameborder= "0" marginwidth= "0" marginheight= "0" style= "border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe>


???

La corrélation mesure le degré d'__association linéaire__ (alignement) du nuage de points.


---
# Corrélation linéaire


.pull-left[

- Coefficient de corrélation de __Pearson__

- Degré d'association __lineaire__

- Pas forcement de __causalité__

- Indépendant de la __pente__

]

.pull-right[
![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Correlation_examples2.svg/600px-Correlation_examples2.svg.png)

.credit[[Wikipedia](https://en.wikipedia.org/wiki/Correlation_and_dependence)]
]


???




---
class: middle

# Merci!

Diapositives créées à l'aide du package R [**xaringan**](https://github.com/yihui/xaringan).

En s'appuyant sur [remark.js](https://remarkjs.com), [**knitr**](https://yihui.org/knitr), et [R Markdown](https://rmarkdown.rstudio.com).

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
