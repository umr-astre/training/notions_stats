---
title: "Modélisation statistique"
author: "Facundo Muñoz<br/>facundo.munoz@cirad.fr<br/>"
institute: "<img src=\"img/CirBlanc_L230px.png\" style=\"width: 25%\" align=\"top\" />"
date: "`r format(Sys.Date())`"
output:
  xaringan::moon_reader:
    # css: ["default", "assets/css/my-theme.css", "assets/css/my-fonts.css"]
    css: ["default", "default-fonts", "libs/font-awesome/css/fontawesome-all.min.css", "assets/css/cirad.css"]
    seal: false
    lib_dir: libs
    nature:
      slideNumberFormat: |
        <div class="progress-bar-container">
          <div class="progress-bar" style="width: calc(%current% / %total% * 100%);">
          </div>
        </div>
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: "16:9"
      beforeInit: "macros.js"
---


```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
knitr::opts_chunk$set(
  collapse = TRUE,
  echo = FALSE,
  fig.retina = 3,
  fig.width=5,
  fig.height=3,
  out.width='90%'
)
# library(ymlthis)
library(tidyverse)
library(xaringanExtra)
# library(palmerpenguins)
# xaringanExtra::use_panelset()
xaringanExtra::use_share_again()
xaringanExtra::style_share_again(
  share_buttons = c("twitter", "linkedin", "pocket")
)

theme_set(theme_ipsum())
```

```{r broadcast, echo=FALSE}
xaringanExtra::use_broadcast()
```


class: title-slide, inverse

.pull-left[
# `r rmarkdown::metadata$title`

## `r rmarkdown::metadata$subtitle`

### `r rmarkdown::metadata$author` 

![](img/CirBlanc_L230px.png)
]
.pull-right[
![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Simple_random_sampling.PNG/450px-Simple_random_sampling.PNG)
.credit[
[Wikipedia](https://en.wikipedia.org/wiki/Sample_(statistics%29)
]
]
???


```{r load}
clinicaltrial <- read.csv(
  "~/.jamovi/modules/lsj-data/data/clinicaltrial.csv"
)

```

---
layout: true

<a class="footer-link" href="https://umr-astre.pages.mia.inra.fr/training/notions_stats/">Notions de base en statistiques - umr-astre.pages.mia.inra.fr/training/notions_stats/</a>


---

# Inférence statistique

.pull-left[
- __Statistique descriptive__ 

  .small[Résumer, __décrire__ et représenter des observations]


- __Statistique inférentielle__

  .small[Extraire des __conclusions__ (inférences) sur les processus sous-jacents]
]

.pull-right[
![](img/iceberg-statistics.jpg)
]


???

Jusqu'à présent nous avons traité les __statistiques descriptives__, qui portent sur la __description__ de ce qu'on observe.

Nous nous penchons à présent sur l'inférence statistique, qui nous permet de tirer des conclusions sur les mécanismes qui ont généré ces données.



---

# Machine statistique

.pull-left[
- __Machine__ génératrice de données

- __Simulateur__ des processus réels

- __Régler__ les commandes de la machine

- Les réglages qui __reproduisent__ au mieux
les observations nous __apprennent__ des choses sur les processus réels
]

.pull-right[
![](img/machine.gif)
![](img/centralities.gif)
]


---
class: inverse, middle, center

## __Machine__ 🠮 __modèle statistique__

## __Pièces__ 🠮 __distributions de probabilité__

## __Réglages__ 🠮 __paramètres__ du modèle

## __Mesure__ de comparaison 🠮 __vraisemblance__


???



---

# Exemple 0


```{r}
set.seed(20210114)
obsdat <- tibble(x = rnorm(100))
```

.pull-left[
```{r}
ggplot(obsdat, aes(x, after_stat(density))) +
  geom_histogram(bins = 15) +
  labs(title = "Données observées", x = NULL, y = NULL) +
  theme_ipsum(grid = "", axis_text_size = 0)
```
]

.pull-right[
Modèle __Normal__

```{r fig.height = 2}
tar_read(normal_model)
```

2 paramètres : 
- __localisation__ : $\mu$ 
- __dispersion__ : $\sigma$

]

???

Une observation X est générée aléatoirement avec la répartition donnée par la courbe.


---

# Exemple 0

Évaluation de la __vraisemblance__ 

![](img/likelihood.gif)



---
# Propriétés de la Loi Normale

.pull-left[
- Noms : _Normale_, _Gaussienne_, _Cloche_ de Gauss

- _Moyenne_ $\mu$, _Écart type_ $\sigma$

- Support : tous les nombres réels $X \in \mathbb R$. Mais concentre le __68 - 95 - 99.7__ % dans $\mu \pm$ (1, 2, 3) $\sigma$

- Symétrique
  
]

.pull-right[

$$X \sim \mathcal{N}(\mu, \sigma)$$

![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Standard_deviation_diagram.svg/1200px-Standard_deviation_diagram.svg.png)
.credit[[Wikimedia commons](https://commons.wikimedia.org/wiki/File:Standard_deviation_diagram.svg#/media/File:Standard_deviation_diagram.svg)]


]


???

La courbe représente une __densité__ de probabilité. 

Probabilité = aire sous la courbe.


---
# Population et échantillon

.pull-left[
- L'__échantillon__ c'est les donnez que vous avez : c'est __concret__

- La __population__ est l'univers de possibles observations.

- C'est l'__objet__ de vos conclusions : ce qui vous êtes en train de __simuler__ avec votre modèle.

]

.pull-right[
![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Simple_random_sampling.PNG/450px-Simple_random_sampling.PNG)
]

.credit[[Wikipedia](https://en.wikipedia.org/wiki/Sample_(statistics)]


???

Les termes __population__ et __échantillon__ viennent des applications initiales de la statistique, où on s'intéresse véritablement à une échantillon d'un groupe d'individus, comme on le fait toujours lors des enquêtes, par exemple.

Mais ce n'est pas toujours le cas. 

Quand on collecte les données des températures moyennes mensuelles des derniers 40 ans dans l'Arctique, pour évaluer le changement climatique on est aussi en train de prendre un échantillonnage de valeurs de température. __Sur quelle population__ ?

Au bien la production d'une certaine variété de riz lors de plusieurs saisons...



---

<iframe width="640px" height="480px" src="https://forms.office.com/Pages/ResponsePage.aspx?id=kF-vYLTj1keMjubW0rEXuRCH9ysT9IREoqpPGB0Q0PhUQ0NVQ0JKSVBWVzgwT1JVNUU2VkZZUDgwVCQlQCN0PWcu&embed=true" frameborder="0" marginwidth="0" marginheight="0" style="border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe>

---

<iframe width="900px" height= "600px" src= "https://forms.office.com/Pages/AnalysisPage.aspx?AnalyzerToken=Xtggujq3bEBNEi7ObhTr5WvdABQtibA7&id=kF-vYLTj1keMjubW0rEXuRCH9ysT9IREoqpPGB0Q0PhUQ0NVQ0JKSVBWVzgwT1JVNUU2VkZZUDgwVCQlQCN0PWcu" frameborder= "0" marginwidth= "0" marginheight= "0" style= "border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe>


???

Je n'ai pas une réponse précise. C'est discutable.

Certainement pas à tous les arbres, car nous savons que des différentes espèces ont des rythmes de croissance divers.

On peut dire : à tous les arbres qui sont à peu près dans les mêmes conditions, par rapport aux facteurs qui influent sur la croissance.

---
# Facteurs associés et processus d'échantillonnage

.pull-left[
La __population__ dépend ainsi des facteurs qui __influencent__ les observations,
de s'ils ont été __contrôlés__ ou pas, et de la méthode d'__échantillonnage__.
]

.pull-right[
```{r facteurs-confusion, fig.width = 5}
tribble(
  ~x, ~y, ~label,
  0, 0, "Croissance",
  -1, 1, "Espèce",
  1, 1, "Qualité\ndu sol",
  -1, -1, "Age",
  0, -1.5, "Climat",
  1, -1, "Pente"
) %>% 
  ggplot(aes(x, y, label = label)) +
  geom_label(
    size = 5,
    fill = c("grey60", rep("lightgrey", 5))
  ) +
  geom_curve(
    data = tribble(
      ~x0, ~y0, ~x1, ~y1,
      -.6,   1, -.2,  .4,  # Espèce
       .6,   1,  .2,  .4,  # QdS
      -.9, -.6, -.5,   0,  # Age
        0,  -1,   0, -.3,  # Climat
       .7,  -1,  .3, -.3   # Pente
    ),
    aes(x = x0, y = y0, xend = x1, yend = y1),
    inherit.aes = FALSE,
    curvature = -.3,
    arrow = arrow(length = unit(0.03, "npc"))
  ) +
  lims(x = c(-2, 2), y = c(-2, 2)) +
  theme_void()
```
]


???

C'est là qu'on doit discuter avec l'expert du domaine.

La discussion porte sur quelles facteurs peuvent vraisemblablement __influencer__ la croissance et jusqu'à quel point.

L'année (le climat) peut être négligé, s'il n'y a pas eu des évènements climatiques extraordinaires. Mais les conclusions porteront également sur les années à climat "normal".

Le site de plantation peut être étendue à d'autres sites avec des conditions de type sol, altitude, pente, etc. similaires.

S'il y a d'autres facteurs qui varient sur la plantation (ensoleillement, type de sol, etc.), soit on les __contrôle__ en enregistrant leur valeur pour chaque observation et en les modélisant son influence, soit on les __randomise__ (ce qui augmentera la variabilité de la partie non-expliqué du modèle)


La __méthode d'échantillonnage__ n'a pas besoin d'être strictement aléatoire, mais il faut justifier qu'elle n'introduit pas des __biais__ sur le phénomène qui nous concerne.






---



.pull-left[
.tiny[
```{r table-clinicaltrial}
kable(clinicaltrial)
```
]]

.pull-right[
# Exemple 1 : Clinical Trial
Est-ce possible utiliser le modèle Normal précédent ?
```{r}
clinicaltrial %>% 
  ggplot(aes(mood.gain)) +
  geom_histogram(bins = 13) +
  labs(y = NULL) +
  theme_ipsum(
    grid = "",
    plot_margin = margin(0,0,0,0),
    axis = FALSE
  ) +
  theme(
    axis.text.y = element_blank()
  )

```
]

???

- Ne ressemble pas à une Normale, mais juste 18 observations !!


---
Voici quelques échantillons de __taille 18__ au hasard d'une vrai loi __Normale__

```{r}
set.seed(20210115)
tibble(rep = 1:9) %>% 
  mutate(
    sample = pmap(., ~rnorm(18, mean = mean(clinicaltrial$mood.gain), sd = sd(clinicaltrial$mood.gain))),
  ) %>% 
  unnest(col = sample) %>% 
  ggplot(aes(sample)) +
  geom_histogram(bins = 13) +
  facet_wrap(~rep, ncol = 3, scales = "free") +
  labs(x = NULL, y = NULL) +
  theme_ipsum(grid = "", axis_text_size = 0, plot_margin = margin(0,0,0,0))
```

???

Continuez-vous à penser que le modèle Normal n'est pas adapté ?

- Surtout parce qu'on a trois groupes pour lesquels on a des raisons de trouver des différences


---
# Tests d'hypothèse

- Nous venons d'effectuer un __test d'hypothèse__ statistique, de façon __visuelle__

- Hypothèse __nulle__ : _Observations Normales_

- __Sous__ l'hypothèse nulle, on peut s'attendre à voir des données qui ressemblent celles de la figure précédente

- Dans une échelle entre __0 (absolument différente)__ et __1 (parfaitement ressemblant)__, à combien évalueriez-vous la __similarité__ avec les données observées ?

--

- Vous venez de juger une __p-valeur__ à l'œil !

- Le Test de Shapiro-Wilk donne $p =$ `r round(shapiro.test(clinicaltrial$mood.gain)$p.value, 2)`



???

On s'avance un peu, me je profite l'occasion.

La seule chose qui manque pour formaliser ce test est de prendre une __mesure__ numérique qui __caractérise__ la distribution (le "__statistique__")

La valeur de __p__ se calcule alors en termes de cette mesure.

- Vérifiez dans __jamovi__

Nous creuserons sur l'interprétation des tests d'hypothèses s'il y a le temps et l'intérêt.

---

# Exemple 1 : Conclusions

- Plusieurs modèles alternatifs peuvent être __compatibles__ avec un jeu de données

- Les tests ne substituent pas le __jugement__ scientifique.

- Il n'y a pas de recettes ni des procédures mécaniques. Juste des __principes__, des __critères__ et des __compromis__.


???

D'autres modèles statistiques, Uniforme, exponentiel auraient été également vraisemblables


---

# Récapitulatif de conceptes

.pull-left[
- Statistique __descriptive__ et __inférentielle__

- __Modèle__ statistique

- __Distributions__ de probabilité

- __Paramètres__ d'un modèle

- __Vraisemblance__
]

--

.pull-right[
- Distribution / Loi __Normale__

- __Population__ et échantillon

- __Test d'hypothèse__

- __Hypothèse nulle__

- __p-valeur__
]


???

Demander aux participants qu'expliquent chaque un un de ces concepts avec ses propres mots.



---
class: middle

# Merci!

Diapositives créées à l'aide du package R [**xaringan**](https://github.com/yihui/xaringan).

En s'appuyant sur [remark.js](https://remarkjs.com), [**knitr**](https://yihui.org/knitr), et [R Markdown](https://rmarkdown.rstudio.com).

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
