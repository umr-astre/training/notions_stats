---
title: "Introduction"
description: |
  Introduction préalable au stage de formation _Notions de base en statistiques_
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```


```{r meta, echo=FALSE}
library(metathis)
meta() %>%
  meta_general(
    description = "Stage de formation. Cirad. Mai 2023.",
    generator = "xaringan and remark.js"
  ) %>% 
  meta_name("gitlab-repo" = "https://forgemia.inra.fr/umr-astre/training/notions_stats") %>% 
  meta_social(
    title = "Notions de base en statistiques",
    url = "https://umr-astre.pages.mia.inra.fr/training/notions_stats/",
    image = "https://umr-astre.pages.mia.inra.fr/training/notions_stats/img/course_icon.svg",
    image_alt = "Logo formation",
    og_type = "website",
    twitter_card_type = "summary_large_image"
  )
```


# Ouverture du stage

Support pour la présentation des participants, de l'intervenant et des contenus de la formation.


```{r share-again}
xaringanExtra::use_share_again()
```

```{r embed-xaringan-presentation, fig.cap="[Slides](slides/S1.1_presentation.html)"}
xaringanExtra::embed_xaringan(url = "slides/S1.1_presentation.html", ratio = "16:9")
```



# Qu'est-ce que la Statistique. Vocabulaire de base.

```{r embed-xaringan-statistique, fig.cap="[Slides](slides/S1.2_statistique.html)"}
xaringanExtra::embed_xaringan(url = "slides/S1.2_statistique.html", ratio = "16:9")
```


# Aperçu de la formation

```{r embed-xaringan-formation, fig.cap="[Slides](slides/S1.3_formation.html)"}
xaringanExtra::embed_xaringan(url = "slides/S1.3_formation.html", ratio = "16:9")
```
