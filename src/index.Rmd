---
title: "Notions de base en statistiques"
# author:
#   - name: "Facundo Muñoz"
#     url: https://www.uv.es/famarmu/
#     affiliation: UMR ASTRE - Cirad
#     affiliation_url: https://umr-astre.cirad.fr/
date: "`r Sys.Date()`"
description: |
  Stage de formation initiale en Statistiques. Mai 2023.
site: distill::distill_website
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)

# Learn more about creating websites with Distill at:
# https://rstudio.github.io/distill/website.html

```



```{r meta, echo=FALSE}
library(metathis)
meta() %>%
  meta_general(
    description = "Stage de formation. Cirad. Mai 2023.",
    generator = "xaringan and remark.js"
  ) %>% 
  meta_name("gitlab-repo" = "https://forgemia.inra.fr/umr-astre/training/notions_stats") %>% 
  meta_social(
    title = "Notions de base en statistiques",
    url = "https://umr-astre.pages.mia.inra.fr/training/notions_stats/",
    image = "https://umr-astre.pages.mia.inra.fr/training/notions_stats/img/course_icon.svg",
    image_alt = "Logo formation Notions de base en statistiques by Facundo Muñoz",
    og_type = "website",
    twitter_card_type = "summary_large_image"
  )
```


# Bienvenus

> À l'issue du stage vous serez capable de __déterminer__ et __conduire__ les méthodes statistiques plus adaptées pour l'analyse des données issues des plans d'expériences élémentaires, __interpréter__ les résultats correctement et __restituer__ les conclusions pertinentes.

- La prochaine édition de la formation aura lieu les jours 22-23 Mai 2023, de 9 à 17h.

- Il ne restent plus de places pour cette édition. Veuillez contactez [Laury Caubet](mailto:laury.caubet@cirad.fr) pour manifester votre intérêt sur une prochaine édition.

- [__Veuillez vous préparer__ à l'avance](preparation.html)

<aside>
[<i class="fas fa-download"></i> Téléchargez toutes les diapositives](slides/Notions-de-base_slides.zip)
</aside>


# Objectifs

<aside>
![](img/course_logo.png)
</aside>



1. Gérer, traiter et organiser les données.

2. Identifier les variables relevantes, leur rôle et leur nature.

3. Décrire les variables graphiquement et avec des paramètres descriptifs.

4. Sélectionner des modèles statistiques adaptés, vérifier leurs hypothèses et les mettre en œuvre.

5. Interpréter les résultats de l'inférence et des tests d'hypothèse fréquentistes.

6. Extraire et communiquer les conclusions. Élaborer sur les sujets de causalité, association et incertitude.


# Programme


1. Gestion de données

    - Stocker et organiser des données efficacement et en sécurité
    
    - Compléter les métadonnées relevantes
  
2. Rôle et nature des variables

    - Établir les rôles explicatifs et réponse des variables
    - Discriminer les échelles de mesure numériques, catégorielles et ordinales

3. Statistique descriptive

    - Calculer des paramètres descriptifs des variables en fonction de leur nature
    - Résumer l’intérêt et les limitations des paramètres descriptifs
    - Représenter graphiquement les données en fonction de leur nature
    - Choisir des palettes de couleurs effectives

4. Inférence statistique

    - Modéliser les relations entre variables à l’aide d’un modèle de régression linéaire
    - Formuler les tests classiques de corrélation, de comparaison de moyennes et d’analyse de données catégorielles, paramétriques et non-paramétriques, en termes d’un modèle de régression linéaire
    - Interpréter les résultats d’un modèle de régression linéaire
    - Distinguer causalité et corrélation
    - Distinguer signification et importance

## Remerciements {.appendix}

This is website made with the [distill package](https://rstudio.github.io/distill/) and set up following ideas from Alison Hill's RStudio workshop [_Teaching in Production_](https://alison.rbind.io/project/rstudio-tip/).

