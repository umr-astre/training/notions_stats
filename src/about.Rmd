---
title: "Notions de base en statistiques"
description: |
  À propos de ce stage de formation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)

library(grid)
```


# Matériaux de référence et supplémentaires

Nous allons utiliser le logiciel [Jamovi](https://www.jamovi.org/) pour la formation. Pour référence, voici un manuel en ligne et en Français :

- [_Apprentissage des statistiques avec Jamovi : Un tutoriel pour les étudiants en psychologie et autres débutants_](https://jmeunierp8.github.io/ManuelJamovi/)

  Danielle Navarro (University of New South Wales), David Foxcroft (Oxford Brookes University), Jean-Marc Meunier (Traducteur, Université Paris 8).


Cette formation propose un approche centré autour de la modélisation inspiré par [_Modeling First: Applying Learning Science to the Teaching of Introductory Statistics_ Son et al. (2020)](https://www.tandfonline.com/doi/full/10.1080/10691898.2020.1844106). 

Des références appropriées (et accessibles en ligne, gratuitement, mais en anglais) pour creuser et supplémenter les contenus peuvent être :

- [_Introduction to Statistics: A Modeling Approach_](https://coursekata.org/preview/default/program)

  CourseKata.org: a project of the UCLA Psychology Department’s Teaching and Learning Lab. 


- [_Statistical Thinking for the 21st Century : An open source textbook for statistics, with companions for R and Python_](https://statsthinking21.org/)

  Russell Poldrack, Stanford University.


- [_Common statistical tests are linear models (or: how to teach stats)_](https://lindeloev.github.io/tests-as-linear/)

  Jonas Kristoffer Lindeløv. Aalborg University, Denmark


Cet autre livre accessible gratuitement (en PDF) avec un approche plus traditionnel, mais d'excellente qualité et plus exhaustif peut vous servir de référence future pour consultation.

- [_OpenIntro Statistics_](https://leanpub.com/openintro-statistics)

  David Diez, Mine Cetinkaya-Rundel, Christopher Barr, and OpenIntro


Voici quelques ressources supplémentaires et gratuits pour creuser sur des sujets qu'on a à peine effleuré

### Visualisation et graphiques

- [Chapitre sur les principes de la visualisation de données](https://rafalab.github.io/dsbook/data-visualization-principles.html) (en anglais), chez [_Introduction to Data Science: Data Analysis and Prediction Algorithms with R_](https://rafalab.github.io/dsbook/). Rafael A. Irizarry.

- Stephanie J. Spielman [Common types of data visualizations](https://rowanbiosci.shinyapps.io/types_of_plots/). Application intéractive en ligne.

- Claus O. Wilke [Fundamentals of Data Visualization](https://clauswilke.com/dataviz/). Livre en ligne.

- [From Data to Viz](https://www.data-to-viz.com/). Guide en ligne pour choisir une visualisation adaptée aux type de données.




# Intervenant

[
<img src="img/fmunoz.jpg" style="
  width: 10%; 
">
](https://www.uv.es/famarmu/)
[__Facundo Muñoz__](https://www.uv.es/famarmu/)
```{r fig.height = .2, fig.width = 2}
grid.draw(textGrob("facundo.munoz@cirad.fr"))
```


- Mathématicien, PhD Statistiques (U. València, Espagne. 2013)

- Post-doc INRA, Orléans, France (2013 - 2017). Génétique Forestière.

- Biostatisticien UMR - ASTRE, Cirad. (2017 - .).

- Statistiques appliquées. Epidémiologie. Modélisation spatio-temporelle. Méthodes Bayésiennes. Programmation R. Formation.


## Remerciements {.appendix}

Cette formation a été préparée et conduite avec l'appui et le soutien de l'équipe _Formation_ du Cirad : Catherine GRIMAL, Lucie LEMAGNEN, Élodie LIEVRE, Emmanuelle MAZZINI et Magali DUFOUR.

Merci aussi à Béatrice RHINO, conceptrice et responsable antérieure de la formation pour le  partage désintéressé de son expérience et matériaux.
