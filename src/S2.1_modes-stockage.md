# Séquence 2.1. Comment sont typiquement stockées vos données ?

1.Quel système de stockage ?
Base de données relationnelle (E.g. MySQL, Oracle, MS Access)
Feuille de calcul (E.g. MS Excel, LibreOffice Calc)
Fichiers de texte (E.g. CSV, JSON)
Autre

2.Quel modèle de données ?
Tableau
Matrice
Liste

3.Quel moyen de diffusion ?
Lien vers un serveur où les données sont centralisées
Copie en pièce jointe par é-mail
