# Notions de base en statistiques

## Objectifs

Distanciel. Planifier en demies journées, étalées sur quatre jours. Après-midis pour participants de Guadeloupe.

Extraits du catalogue de formation : https://www.cirad.fr/enseignement-formation

1. Comprendre le vocabulaire de base employé en statistique

2. Construire une base de données pour une analyse statistique

3. Connaître les principales lois de distribution

4. Calculer et interpréter les différents indicateurs statistiques

5. Décrire et représenter graphiquement des séries quantitatives ou qualitatives

6. Maîtriser les tests statistiques élémentaires sur un ou deux échantillons

L'objectif principal est de développer la culture statistique dans le sens de Rumsey (2002), en utilisant un approche "Modeling First" qui favorise la compréhension approfondie et des connaissances transférables (Son et al. 2020).

J'aime bien l'approche visuel à la modélisation adopté chez [^1] dans lequel on fait des graphiques des données pour comprendre les modèles.


- Objectif __opérationnel__ du stage :

	> À l'issue du stage les participants seront capables de __déterminer__ et __conduire__ les méthodes statistiques plus adaptées pour l'analyse des données issues des plans d'expériences élémentaires, __interpréter__ les résultats correctement et __restituer__ les conclusions pertinentes.

- Objectifs pédagogiques :

	1. Gérer, traiter et organiser les données.

	2. Identifier les variables relevantes, leur rôle et leur nature.

	3. Décrire les variables graphiquement et avec des paramètres descriptifs.

	4. Sélectionner des modèles statistiques adaptés, vérifier leurs hypothèses et les mettre en œuvre.

		4.1 Modéliser les relations entre variables à l‘aide d‘un modèle de régression linéaire

		4.2 Formuler les tests classiques de corrélation, de comparaison de moyennes et d‘analyse de données catégorielles, paramétriques et non-paramétriques, en termes d‘un modèle de régression linéaire

	5. Interpréter les résultats de l'inférence et des tests d'hypothèse fréquentistes.

	6. Extraire et communiquer les conclusions. Élaborer sur les sujets de causalité, association et incertitude.


[^1]: [https://coursekata.org/preview/version/0ea12f05-dce8-4b7c-a6ad-5950ec4a57bb/lesson/6/0](https://coursekata.org/preview/version/0ea12f05-dce8-4b7c-a6ad-5950ec4a57bb/lesson/6/0)


## Infrastructure logistique

- Se voir et s'entendre correctement. MS Teams.

- Partager un tableau à dessiner par l'intervenant. White board tool.

- Proposer des quizz. Forms tool.


## Méthode d'évaluation

- Évaluation continue, suite à chaque module par des quizz et ou exercices.

- Né pas utiliser le terme "évaluation". Quizz, feedback, retour.


## References

Son, J.Y., Blake, A.B., Fries, L., Stigler, J.W., 2020. Modeling First: Applying Learning Science to the Teaching of Introductory Statistics. Journal of Statistics Education 1–23. https://doi.org/10.1080/10691898.2020.1844106
