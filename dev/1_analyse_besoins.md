# Analyse de besoins

Extraits du catalogue de formation : https://www.cirad.fr/enseignement-formation

- Permettre aux participants de __s'approprier__ des notions statistiques élémentaires pour __analyser__ leurs données et __restituer__ leurs résultats.

- Le cours sera essentiellement __pratique__ et __interactif__, les participants pourront se munir de leurs propres données.

- Le Programme de cette formation peut être __adapté__ sur mesure en fonction des besoins et des pays.

- Durée : __2 jours__

- Public : Chercheurs - Ingénieurs - Techniciens - Doctorants - Salariés du privé. __Novices__ en statistiques, avec un peu d'exposition.

- Prérequis : Maitrise d’un __tableur__ (type Excel), Maîtrise de la langue française, Anglais (notions)

- Retours des évaluations de la dernière session de Décembre 2019 :
	- Les plus de la formation : Formations à la carte; La formatrice s'est adaptée aux attentes de chacun - Remise à plat et clarification des notions de base - Le déroulé de comment faire un test statistique - langage à acquérir - les exemples concrets - Lien entre théorie et pratique

	- Besoins complémentaires exprimés : Analyses multivariées et sous R - ANOVA - GLM - avoir plus de temps pour de la pratique et accompagnement personnalisé - travailler sur des logiciels libres (infostat) pour travailler plus facilement avec les partenaires du Sud - formation en statistique multi-dimensionnelle; bases de R pour pouvoir utiliser les scripts


- Besoins spécifiques des participants :

	- Analyser l’impact de certains facteurs sur la qualité d'un produit (données d'expériences)

	- Sélectionner le test statistique approprié pour conduire l’étude demandée (types de variables, objectifs d'inférence)

	- Comprendre les résultats et graphes obtenus pour pouvoir les interpréter (interprétation des p-values et tests d'hipothèse)

	- Donner des conclusions et recommandations sur la base des résultats et graphes obtenus (restitution, conclusions, incertitude)