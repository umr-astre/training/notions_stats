<map version="1.1.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1593766572476" ID="ID_1816335895" MODIFIED="1593766673212" TEXT="Besoins &quot;Notions de base en Statistiques&quot;">
<font NAME="Open Sans" SIZE="12"/>
<node CREATED="1593768312975" ID="ID_169446444" MODIFIED="1593768316684" POSITION="right" TEXT="Pratique"/>
<node CREATED="1593768318177" ID="ID_260934741" MODIFIED="1593768321190" POSITION="right" TEXT="Int&#xe9;ractif"/>
<node CREATED="1593768322005" ID="ID_1078137902" MODIFIED="1593768324266" POSITION="right" TEXT="Adapt&#xe9;"/>
<node CREATED="1593768341358" ID="ID_467854361" MODIFIED="1593768355047" POSITION="left" TEXT="S&apos;approprier des notions &#xe9;l&#xe9;mentaires"/>
<node CREATED="1593768371767" ID="ID_1935107170" MODIFIED="1593768377220" POSITION="left" TEXT="Analyser donn&#xe9;es"/>
<node CREATED="1593768377780" ID="ID_1903903895" MODIFIED="1593768380220" POSITION="left" TEXT="Restituer"/>
</node>
</map>
