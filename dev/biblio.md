# Bibliographic and informatic ressources

## For preparing material (not necessarily addressed to students)

Common statistical tests are linear models (or: how to teach stats)
https://lindeloev.github.io/tests-as-linear/

Learning Statistics with JASP: A Tutorial for Psychology Students and Other Beginners
Danielle J. Navarro, David R. Foxcroft, and Thomas J. Faulkenberry
https://learnstatswithjasp.com/
English

Introduction to Statistics Using LibreOffice.org Calc
http://www.comfsm.fm/~dleeling/statistics/text.html
English

Teaching Statistics: A Bag of Tricks
Andrew Gelman, Deborah Nolan
English

Understanding Statistics Using R
Randall SchumackerSara Tomek
English

Undergraduate Statistics with JASP
https://osf.io/t56kg/

Interactive book for an introductory statistics course
(based on R, but on the web.)
https://coursekata.org/

Statistical Thinking for the 21st Century
https://statsthinking21.github.io/statsthinking21-core-site/

Data Organization in Spreadsheets - Data Carpentry lesson
https://datacarpentry.org/spreadsheet-ecology-lesson/


## Interactive websites

Book Of Apps for Statistics Teaching
https://sites.psu.edu/shinyapps/

## Software packages

See src/statistical_software.R for the research on available software from Wikipedia


- ADaMSoft
Dataplot
ELKI
gretl
Julia
- JASP (https://jasp-stats.org/)

- OpenEpi (http://www.openepi.com/). Online calculations (no installation needed (can be run off-line as a webpage)!! Available in french. But no regression.

Orange
PSPP
R
RKWard
ROOT
SageMath

- Salstat (https://www.salstat.com/tutorial-install-windows.html) Difficult installation for Windows (install python and other dependencies, download source, etc.)

SOCR
SOFA Statistics

Other packages not listed in the Wikipedia article

- EpiDat (https://www.sergas.es/Saude-publica/EPIDAT). Xunta de Galicia. Seems out-dated (last update in 2016). Windows only? Web non-responsive.

- jamovi (https://www.jamovi.org/)  Fork of JASP built over more modern framework with more focus on frequentist stats.
