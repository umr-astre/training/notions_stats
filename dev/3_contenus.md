# Contenus


## Introduction formation


### S1.1

- Présentation des participants

	Tour de table. À volonté.

	Nom, activité. Attentes et objectifs formation.

- Présentation intervenant

- Questionnaire initial

- Règles et normes de la formation. Méthode d'évaluation et réussite. Enregistrement.

- Lexique de termes



### S1.2

- Qu'est-ce que la Statistique ?

	Brain-storming de termes et concepts évoqués par cette question.

	Termes attendus : variation, échantillonnage, données, distribution, probabilité, Normal, variable, modèle, descriptive, inférence, observations, population, hypothèses, individus, paramètres

	Intégrer les termes dans un cadre conceptuel. Discuter la signification des termes.


- Exemple complet pour présentation de la formation



## S4.1
