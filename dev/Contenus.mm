<map version="1.1.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1596016531792" ID="ID_871282758" MODIFIED="1596199526192">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="6">Notions de base </font>
    </p>
    <p>
      <font size="6">en statistiques</font>
    </p>
  </body>
</html></richcontent>
<font NAME="Fira Sans" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1596199922482" ID="ID_1605295646" MODIFIED="1596200423529" POSITION="right" TEXT="4. Inf&#xe9;rence statistique">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1596199942854" ID="ID_1768548792" MODIFIED="1607521112114" TEXT="&#x2022; Mod&#xe9;liser les r&#xe9;lations entre variables &#xe0; l&#x2018;aide d&#x2018;un mod&#xe8;le de r&#xe9;gression lin&#xe9;aire">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596031497047" ID="ID_985408543" MODIFIED="1606732329413" TEXT="Le mod&#xe8;le lin&#xe9;aire">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596031507182" ID="ID_1295814282" LINK="https://lindeloev.github.io/tests-as-linear/#1_the_simplicity_underlying_common_tests" MODIFIED="1596200049762" TEXT="https://lindeloev.github.io/tests-as-linear/#1_the_simplicity_underlying_common_tests">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596122759208" ID="ID_146177667" MODIFIED="1596200049763" TEXT="Le coefficient de d&#xe9;termination">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1596121383570" FOLDED="true" ID="ID_639420723" MODIFIED="1606732345366">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Corr&#233;lations et r&#233;gression
    </p>
    <p>
      (Y, X continues)
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596121478442" ID="ID_490941150" MODIFIED="1596200080732" TEXT="Coefficients de corr&#xe9;lation de Pearson et Spearman">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1596029081152" ID="ID_372423734" MODIFIED="1606732352432" TEXT="Distributions de probabilit&#xe9;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596029205452" ID="ID_1696812690" MODIFIED="1596200273567" TEXT="Normal">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596029218094" ID="ID_1192987922" MODIFIED="1596200273567" TEXT="Binomial">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596029208692" ID="ID_310362365" MODIFIED="1596200273568" TEXT="Poisson">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1596199942856" FOLDED="true" ID="ID_1725681482" MODIFIED="1607521098308" TEXT="&#x2022; Formuler les tests classiques de corr&#xe9;lation, de comparaison de moyennes et d&#x2018;analyse de donn&#xe9;es cat&#xe9;gorielles, param&#xe9;triques et non-param&#xe9;triques, en termes d&#x2018;un mod&#xe8;le de r&#xe9;gression lin&#xe9;aire">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596120979885" ID="ID_1315583201" MODIFIED="1606738073176">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comparaison de moyennes
    </p>
    <p>
      (Y continue, X cat&#233;gor.)
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596120987046" ID="ID_948419126" MODIFIED="1596200290438" TEXT="t-test une et deux &#xe9;chantillons">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596123225909" FOLDED="true" ID="ID_514906082" MODIFIED="1606738096111" TEXT="Comparaison de moyennes de plusieurs groupes (ANOVA)">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1596124396787" ID="ID_301596044" MODIFIED="1596185291816" TEXT="Multiple comparisons and post-hoc tests"/>
</node>
<node COLOR="#111111" CREATED="1596121130148" ID="ID_144438899" MODIFIED="1596200290438" TEXT="Welch test (diff&#xe9;rentes sd)">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596121178852" ID="ID_865118359" MODIFIED="1596200290438" TEXT="t-test &#xe9;chantillons par&#xe9;es">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596121311691" ID="ID_76925433" MODIFIED="1596200290438" TEXT="Test de normalit&#xe9;">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596121343146" ID="ID_1214729636" MODIFIED="1606738680078" TEXT="Tests non-param&#xe9;triques">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1596124358252" ID="ID_1100428976" MODIFIED="1596185291818" TEXT="Kruskall-Wallis"/>
</node>
<node COLOR="#111111" CREATED="1596124443854" ID="ID_1476014039" MODIFIED="1596200290438" TEXT="Multiple regression and multiple-way ANOVA">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1596120737431" FOLDED="true" ID="ID_921140124" MODIFIED="1596200422002">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Analyse de donn&#233;es cat&#233;goriels
    </p>
    <p>
      (Y, X cat&#233;gorielles)
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596120748886" ID="ID_1200123911" MODIFIED="1596200298976" TEXT="Test de Chi-carr&#xe9;">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596120761950" ID="ID_1450944598" MODIFIED="1596200298977" TEXT="Test d&apos;ind&#xe9;pendence ou association">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1606730320903" ID="ID_407365980" MODIFIED="1606748396657" POSITION="right" TEXT="5. Interpr&#xe9;tation de r&#xe9;sultats">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1596199942857" ID="ID_1874338393" MODIFIED="1606730389150" TEXT="&#x2022; Mod&#xe8;le de regression lin&#xe9;aire">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596108599664" ID="ID_1119251035" MODIFIED="1596185291798" TEXT="Probabilit&#xe9; et statistique">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596029041801" ID="ID_1417495396" MODIFIED="1596185291799" TEXT="Mod&#xe8;les statistiques">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596108649271" FOLDED="true" ID="ID_755151613" MODIFIED="1606730252115" TEXT="Le Th&#xe9;or&#xe8;me de la Limite Central">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596118097852" ID="ID_94137184" MODIFIED="1596185291800" TEXT="La distribution des moyennes des &#xe9;chantillons"/>
</node>
<node COLOR="#990000" CREATED="1596113850586" FOLDED="true" ID="ID_1005188230" MODIFIED="1596200422004" TEXT="Dataset">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596113845989" ID="ID_629096533" LINK="https://www.data.gouv.fr/fr/datasets/donnees-de-consommations-et-habitudes-alimentaires-de-letude-inca-3/" MODIFIED="1596185291800" TEXT="https://www.data.gouv.fr/fr/datasets/donnees-de-consommations-et-habitudes-alimentaires-de-letude-inca-3/"/>
</node>
<node COLOR="#990000" CREATED="1596113961760" FOLDED="true" ID="ID_1738116375" MODIFIED="1596200422005" TEXT="Sofware">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596113966878" ID="ID_885098542" MODIFIED="1596185291803" TEXT="Excel / Calc"/>
<node COLOR="#111111" CREATED="1596113971118" FOLDED="true" ID="ID_1192319630" MODIFIED="1596200422004" TEXT="JASP / jamovi">
<node COLOR="#111111" CREATED="1596114103463" ID="ID_1071155011" LINK="https://jasp-stats.org/" MODIFIED="1596185291804" TEXT="https://jasp-stats.org/"/>
<node COLOR="#111111" CREATED="1596114057958" ID="ID_916504852" LINK="https://www.jamovi.org/" MODIFIED="1596185291804" TEXT="https://www.jamovi.org/"/>
</node>
<node COLOR="#111111" CREATED="1596117644862" FOLDED="true" ID="ID_739891573" MODIFIED="1596200422005" TEXT="PSPP">
<node COLOR="#111111" CREATED="1596117710958" ID="ID_1494505951" LINK="https://www.gnu.org/software/pspp/" MODIFIED="1596185291804" TEXT="https://www.gnu.org/software/pspp/"/>
</node>
<node COLOR="#111111" CREATED="1596117722582" ID="ID_834828062" MODIFIED="1596185291805" TEXT="J&apos;ai peur de cr&#xe9;er de la confusion en utilisant plusieurs outils"/>
</node>
<node COLOR="#990000" CREATED="1596026086296" ID="ID_701085339" LINK="https://seeing-theory.brown.edu/" MODIFIED="1596185291805" TEXT="https://seeing-theory.brown.edu/">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596117128003" FOLDED="true" ID="ID_1606727504" MODIFIED="1596200422005" TEXT="Tests d&apos;Hypoth&#xe8;ses">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596124557706" ID="ID_1977750559" MODIFIED="1596200324701" TEXT="Error types and statistical power">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596124592857" ID="ID_642628077" MODIFIED="1596200324701" TEXT="Intervals de confiance">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1606748399290" ID="ID_537855540" MODIFIED="1606748410902" TEXT="Intervals de confiance">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1596121205891" ID="ID_1409604173" MODIFIED="1606748423406" TEXT="Taille de l&apos;effet (Cohen&apos;s d)">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1606748431211" ID="ID_1856181486" MODIFIED="1606748438527" TEXT="Puissance statistique">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1606730328782" ID="ID_1589533542" MODIFIED="1606730368285" POSITION="right" TEXT="6. Communication de conclusions">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1596199942858" ID="ID_1121566620" MODIFIED="1596200424254" TEXT="&#x2022; Distinguer causalit&#xe9; et corr&#xe9;lation">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596185213877" ID="ID_222307201" MODIFIED="1596200350160" TEXT="Reporting model&apos;s results">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1596199942859" ID="ID_145145171" MODIFIED="1596200424254" TEXT="&#x2022; Distinguer significativit&#xe9; et importance">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596124632834" ID="ID_1602906181" MODIFIED="1596200347543" TEXT="p-values et significativit&#xe9;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1596199186087" ID="ID_741163978" MODIFIED="1596200423530" POSITION="left" TEXT="1. Gestion de donn&#xe9;es">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1596199207552" ID="ID_3385990" MODIFIED="1596200424254" TEXT="Stocker et organiser des donn&#xe9;es efficacement et en s&#xe9;curit&#xe9;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596016905235" ID="ID_698956397" MODIFIED="1610120308567" TEXT="Principes de s&#xe9;curit&#xe9; et r&#xe9;productibilit&#xe9;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596017508429" ID="ID_662378542" MODIFIED="1596185291840" TEXT="Gestion de versions des fichiers"/>
<node COLOR="#111111" CREATED="1596021054320" ID="ID_1798176916" MODIFIED="1596185291840" TEXT="Noms de fichiers"/>
<node COLOR="#111111" CREATED="1596021280279" ID="ID_1969002319" MODIFIED="1596185291841" TEXT="Formats de fichiers"/>
<node COLOR="#111111" CREATED="1596017547335" ID="ID_1843436134" MODIFIED="1596185291841" TEXT="&#xc9;viter duplications"/>
<node COLOR="#111111" CREATED="1596017578670" ID="ID_1250892266" MODIFIED="1596185291841" TEXT="Ne toucher pas les donn&#xe9;es source"/>
<node COLOR="#111111" CREATED="1596017594863" ID="ID_272686988" MODIFIED="1596185291841" TEXT="S&#xe9;parer stockage et analyse"/>
<node COLOR="#111111" CREATED="1596017654745" ID="ID_789545046" MODIFIED="1596185291841" TEXT="Tra&#xe7;abilit&#xe9;: documenter la source"/>
<node COLOR="#111111" CREATED="1596021392473" ID="ID_195016166" LINK="https://rrcns.readthedocs.io/en/latest/" MODIFIED="1596199617183" TEXT="https://rrcns.readthedocs.io/en/latest/">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1596020843739" FOLDED="true" ID="ID_341948423" MODIFIED="1606727714204" TEXT="Principes d&apos;organisation">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596020862763" ID="ID_268889332" MODIFIED="1596199066825" TEXT="&quot;Tidy&quot; data"/>
<node COLOR="#111111" CREATED="1596021095521" ID="ID_1393461673" MODIFIED="1596185291842" TEXT="Noms des variables"/>
<node COLOR="#111111" CREATED="1596021683375" ID="ID_1759792685" LINK="https://r4ds.had.co.nz/tidy-data.html#tidy-data-1" MODIFIED="1596185291842" TEXT="https://r4ds.had.co.nz/tidy-data.html#tidy-data-1"/>
</node>
<node COLOR="#990000" CREATED="1596199216897" ID="ID_603658463" MODIFIED="1610120520863" TEXT="M&#xe9;tadonn&#xe9;es">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596020963270" ID="ID_1852826828" MODIFIED="1610116477374" TEXT="Description des variables">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1596021112354" ID="ID_1315958538" MODIFIED="1610116477375" TEXT="Type, cat&#xe9;gories/rang, valeurs manquantes">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1596199656371" ID="ID_508785208" MODIFIED="1596200423531" POSITION="left" TEXT="2. R&#xf4;le et nature des variables">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1596199689261" ID="ID_1153037556" MODIFIED="1596200424255" TEXT="&#x2022; Etablir les r&#xf4;les explicatif et r&#xe9;ponse des variables">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596023170730" ID="ID_777839483" MODIFIED="1596199706562" TEXT="Variables explicatives et variable r&#xe9;ponse">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1596199693455" ID="ID_1704681284" MODIFIED="1606729580345" TEXT="&#x2022; Discriminer les &#xe9;chelles de mesure num&#xe9;riques, cat&#xe9;gorielles et ordinales">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596023182546" ID="ID_1430664073" MODIFIED="1596199713204" TEXT="Variables num&#xe9;riques, cat&#xe9;gorielles et ordinales">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1596199734355" ID="ID_1888043647" MODIFIED="1596200423533" POSITION="left" TEXT="3. Statistique descriptive">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1596199798234" ID="ID_777471220" MODIFIED="1596200424256" TEXT="&#x2022; Calculer des param&#xe8;tres descriptifs des variables en fonction de leur nature">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596023226659" ID="ID_90166979" MODIFIED="1596185291850" TEXT="Centralit&#xe9;">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596023232116" ID="ID_1012893531" MODIFIED="1596185291850" TEXT="Variabilit&#xe9;">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596023236204" FOLDED="true" ID="ID_1993794889" MODIFIED="1606729643058" TEXT="Corr&#xe9;lation">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596032040047" ID="ID_123221908" LINK="http://guessthecorrelation.com/" MODIFIED="1596185291850" TEXT="guessthecorrelation.com"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1596199761355" ID="ID_1988203935" MODIFIED="1606729751672" TEXT="&#x2022; R&#xe9;sumer l&#x2019;int&#xe9;r&#xea;t et les limitations des param&#xe8;tres descriptifs">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596023326021" ID="ID_1780868596" MODIFIED="1596199824563" TEXT="Anscombe&apos;s quartet">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596023475889" ID="ID_1144578991" LINK="https://janhove.github.io/teaching/2016/11/21/what-correlations-look-like" MODIFIED="1596199824563" TEXT="https://janhove.github.io/teaching/2016/11/21/what-correlations-look-like">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1596199761356" ID="ID_943412498" MODIFIED="1596200424258" TEXT="&#x2022; Repr&#xe9;senter graphiquement les donn&#xe9;es en fonction de leur nature">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596023975811" ID="ID_1276001166" LINK="https://eazybi.com/blog/data_visualization_and_chart_types/" MODIFIED="1596185291852" TEXT="https://eazybi.com/blog/data_visualization_and_chart_types/">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596024611923" ID="ID_680350181" LINK="https://www.data-to-viz.com/" MODIFIED="1596185291852" TEXT="https://www.data-to-viz.com/">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596024834019" ID="ID_8506033" MODIFIED="1606731174624" TEXT="Graphiques r&#xe9;sum&#xe9;s">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1596025597884" ID="ID_1431534467" LINK="https://www.autodeskresearch.com/publications/samestats" MODIFIED="1596185291853" TEXT="https://www.autodeskresearch.com/publications/samestats"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1596199761357" ID="ID_1912966475" MODIFIED="1596200424259" TEXT="&#x2022; Choisir des palettes de couleurs effectives">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1596029955735" ID="ID_535372747" LINK="http://www.perceptualedge.com/articles/visual_business_intelligence/rules_for_using_color.pdf" MODIFIED="1596185291853" TEXT="perceptualedge.com &gt; Articles &gt; Visual business intelligence &gt; Rules for using color">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596030768855" ID="ID_393779139" LINK="https://github.com/njtierney/monash-colour-in-graphics" MODIFIED="1596185291856" TEXT="https://github.com/njtierney/monash-colour-in-graphics">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596030796698" ID="ID_422147714" LINK="http://hclwizard.org/" MODIFIED="1596185291856" TEXT="hclwizard.org">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1596032820688" ID="ID_259054393" LINK="https://color.method.ac/" MODIFIED="1596185291857" TEXT="https://color.method.ac/">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
</node>
</map>
